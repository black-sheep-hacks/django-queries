from django.db import models
from django.contrib.postgres.indexes import GinIndex
from django.core.validators import MaxValueValidator, MinValueValidator


class BookQuerySet(models.QuerySet):
    def with_reviews(self):
        return self.annotate(score=models.Avg('review__score'))

    def with_reviews_per_category(self):
        categories = Category.objects.filter(
            book=models.OuterRef('pk')
        ).annotate(
            cs=models.Avg('book__review__score')
        )
        qs = self.annotate(
            category_score=models.Subquery(categories.values('cs')[:1])
        )
        return qs

    def with_reviews_per_category_window(self):
        return self.annotate(
            category_score=models.Window(
                expression=models.Avg('review__score'),
                partition_by=[models.F('category_id')]
            )
        ).distinct()


class Category(models.Model):
    name = models.CharField(max_length=100)


class Book(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    authors = models.ManyToManyField('Author', related_name='books')
    category = models.ForeignKey('Category', on_delete=models.CASCADE)

    objects = BookQuerySet.as_manager()


class Author(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    class Meta:
        indexes = [
            GinIndex(name='review_author_ln_gin_idx', fields=['last_name'], opclasses=['gin_trgm_ops'])
        ]


class Review(models.Model):
    summary = models.CharField(max_length=200)
    text = models.TextField()
    book = models.ForeignKey('Book', on_delete=models.CASCADE)
    score = models.PositiveSmallIntegerField(
        default=1,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(5)
        ],
    )

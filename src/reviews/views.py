from rest_framework.viewsets import ModelViewSet


from reviews.models import Book, Category, Author, Review
from reviews.serializers import BookSerializer, ReviewSerializer, CategorySerializer


class BookViewSet(ModelViewSet):
    queryset = (
        Book.objects.with_reviews_per_category()
        .order_by('id')
    )
    serializer_class = BookSerializer


class CategoryViewSet(ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class AuthorViewSet(ModelViewSet):
    queryset = Author.objects.all()


class ReviewViewSet(ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer

from rest_framework.routers import SimpleRouter

from reviews.views import AuthorViewSet, BookViewSet, CategoryViewSet, ReviewViewSet

appname = 'reviews'


router = SimpleRouter()
router.register('authors', AuthorViewSet)
router.register('books', BookViewSet)
router.register('categories', CategoryViewSet)
router.register('reviews', ReviewViewSet)

urlpatterns = router.urls

from rest_framework.serializers import ModelSerializer, SerializerMethodField

from reviews.models import Book, Review, Category


class BookSerializer(ModelSerializer):
    class Meta:
        model = Book
        fields = ('id', 'title', 'category', 'category_score',)
    
    category_score = SerializerMethodField()

    def get_category_score(self, book: Book):
        return book.category_score



class ReviewSerializer(ModelSerializer):
    class Meta:
        model = Review
        fields = '__all__'

    def save(self, *args):
        return super().save(*args)


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'

SELECT
    "reviews_book"."id",
    "reviews_book"."title",
    "reviews_book"."description",
    "reviews_book"."category_id",
    (
        SELECT AVG(U2."score") AS "cs"
        FROM "reviews_category" U0
        INNER JOIN "reviews_book" U1 ON (U0."id" = U1."category_id")
        LEFT OUTER JOIN "reviews_review" U2 ON (U1."id" = U2."book_id")
        WHERE U1."id" = "reviews_book"."id"
        GROUP BY U0."id"
        LIMIT 1
    ) AS "category_score"
FROM "reviews_book"


SELECT 
    "reviews_book"."id",
    "reviews_book"."title",
    "reviews_book"."description",
    "reviews_book"."category_id",
    (
        SELECT AVG("reviews_review"."score") AS "cs"
        FROM "reviews_category"
        INNER JOIN "reviews_book" "reviews_book" ON (
            "reviews_category"."id" = "reviews_book"."category_id")
        LEFT OUTER JOIN "reviews_review" ON (
            "reviews_book"."id" = "reviews_review"."book_id")
        WHERE "reviews_book"."id" = "reviews_book"."id"
        GROUP BY "reviews_category"."id"
        LIMIT 1
    ) AS "category_score"
FROM "reviews_book"
